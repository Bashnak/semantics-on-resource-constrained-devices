package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.mindswap.pellet.jena.ModelExtractor;
import org.mindswap.pellet.jena.ModelExtractor.StatementType;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.UnknownOWLOntologyException;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.util.InferredOntologyGenerator;

import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;
import es.unizar.pellet_231.R;

public class MainActivity extends Activity {

	private OWLReasonerFactory reasonerFactory = null;
	private OWLOntology peopleOntology;
	private static final String ONTOLOGY = "people.owl";
	private TextView myText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		myText = (TextView) findViewById(R.id.text1); // creato in main.xml
		myText.setTypeface(null, Typeface.BOLD_ITALIC);
		myText.append("\n----------App Started----------\n----------Checking Consistency----------");

		this.reasonerFactory = new PelletReasonerFactory();

		InputStream is = null;
		File f = new File(getCacheDir() + "/" + ONTOLOGY);
		if (!f.exists())
			try {

				is = getAssets().open(ONTOLOGY);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		try {
			this.peopleOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(is);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}

		// a config object. Things like monitor, timeout, etc, go here
		OWLReasonerConfiguration config = new SimpleConfiguration(50000);
		// Create a reasoner that will reason over our ontology and its imports
		// closure. Pass in the configuration.
		OWLReasoner reasoner = this.reasonerFactory.createReasoner(this.peopleOntology, config);
		// Ask the reasoner to classify the ontology

		
		reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY, InferenceType.DATA_PROPERTY_HIERARCHY,
				InferenceType.OBJECT_PROPERTY_HIERARCHY, InferenceType.CLASS_ASSERTIONS);


		// We can determine if the ontology is actually consistent (in this
		// case, it should be).
		float startTime = System.nanoTime();
		boolean isConsistent = reasoner.isConsistent();
		float endTime = System.nanoTime();
		float duration = (endTime - startTime) / 1000000;
		System.out.println("\n----------Consistency Checked : " + isConsistent + "----------");
		myText.append("\n.................." + isConsistent + "\n ----------execution time : " + duration
				+ "ms----------\n\n");
		// get a list of unsatisfiable classes
		Node<OWLClass> bottomNode = reasoner.getUnsatisfiableClasses();
		// leave owl:Nothing out
		Set<OWLClass> unsatisfiable = bottomNode.getEntitiesMinusBottom();
		if (!unsatisfiable.isEmpty()) {
			System.out.println("The following classes are unsatisfiable: ");
			for (OWLClass cls : unsatisfiable) {
				System.out.println(cls.getIRI().getFragment());
			}
		} else {
			System.out.println("There are no unsatisfiable classes");
		}
		// Look up and print all direct subclasses for all classes
		myText.append(
				"\n----------Analaysing people.owl Ontology----------\n\n----------Printing All Direct Subclasses for every Class----------\n\n\n");
		startTime = System.nanoTime();
		for (OWLClass c : this.peopleOntology.getClassesInSignature()) {
			// the boolean argument specifies direct subclasses; false would
			// specify all subclasses
			// a NodeSet represents a set of Nodes.
			// a Node represents a set of equivalent classes
			NodeSet<OWLClass> subClasses = reasoner.getSubClasses(c, true);
			for (OWLClass subClass : subClasses.getFlattened()) {
				System.out.println(subClass.getIRI().getFragment() + "\t  subclass of  \t" + c.getIRI().getFragment());

				myText.append("\n" + subClass.getIRI().getFragment() + "\t  subclass of  \t" + c.getIRI().getFragment()
						+ "\n");

			}
		}
		endTime = System.nanoTime();
		duration = (endTime - startTime) / 1000000;
		myText.append("\n----------Execution Time : " + duration + "ms----------\n\n");
		// for each class, look up the instances
		myText.append("\n----------Printing istances and propriety assertions----------\n");
		startTime = System.nanoTime();
		for (OWLClass c : this.peopleOntology.getClassesInSignature()) {
			// the boolean argument specifies direct subclasses; false would
			// specify all subclasses
			// a NodeSet represents a set of Nodes.
			// a Node represents a set of equivalent classes/or sameAs
			// individuals
			NodeSet<OWLNamedIndividual> instances = reasoner.getInstances(c, true);
			for (OWLNamedIndividual i : instances.getFlattened()) {
				System.out.println(i.getIRI().getFragment() + "\t  instance  of \t" + c.getIRI().getFragment());

				myText.append(
						"\n" + i.getIRI().getFragment() + "\t  instance of  \t" + c.getIRI().getFragment() + "\n");

				// look up all property assertions
				for (OWLObjectProperty op : this.peopleOntology.getObjectPropertiesInSignature()) {
					NodeSet<OWLNamedIndividual> petValuesNodeSet = reasoner.getObjectPropertyValues(i, op);
					for (OWLNamedIndividual value : petValuesNodeSet.getFlattened()) {
						System.out.println(i.getIRI().getFragment() + "\t" + op.getIRI().getFragment() + "\t"
								+ value.getIRI().getFragment());

						myText.append("\n" + i.getIRI().getFragment() + "\t" + op.getIRI().getFragment() + "\t"
								+ value.getIRI().getFragment() + "\n");
					}
				}
			}
		}
		endTime = System.nanoTime();
		duration = (endTime - startTime) / 1000000;
		myText.append("\n----------Execution Time : " + duration + "ms----------\n\n");

		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLDataFactory fac = manager.getOWLDataFactory();

		/*
		 * IRI ontoIRI = peopleOntology.getOntologyID().getOntologyIRI();
		 * PrefixManager pm = new DefaultPrefixManager(ontoIRI.toString());
		 * OWLNamedIndividual kevin = fac.getOWLNamedIndividual(":Fido",pm);
		 * myText.append("\n\n\n\n\n\n\n\n\n\n" + kevin +"classes "+
		 * reasoner.getTypes(kevin, false)+"\n\n\n\n\n\n\n\n\n\n" );
		 */

		NodeSet<OWLNamedIndividual> duckinstances = null;
		NodeSet<OWLNamedIndividual> dogistances = null;

		OWLNamedIndividual duckname = null;
		for (OWLClass c : peopleOntology.getClassesInSignature()) {
			if (c.getIRI().getFragment().equals("dog")) { //Change this string to modify the first parameter of query
				duckinstances = reasoner.getInstances(c, false);
				System.out.println(c.getIRI().getFragment());
				myText.append("\n" + c.getIRI().getFragment());
				for (OWLNamedIndividual i : duckinstances.getFlattened()) {
					System.out.println(i.getIRI().getFragment());
					myText.append("\n" + i.getIRI().getFragment());
					duckname = i;
				}
			}
		}
		myText.append("\n\n\n superclassi di: " + duckname + "\n\n\n" + reasoner.getTypes(duckname, false) + "\n\n");

		OWLNamedIndividual dogname = null;
		for (OWLClass c : peopleOntology.getClassesInSignature()) {
			if (c.getIRI().getFragment().equals("tabloid")) { //Change this string to modify the second parameter of query
				dogistances = reasoner.getInstances(c, false);
				System.out.println(c.getIRI().getFragment());
				myText.append("\n" + c.getIRI().getFragment());
				for (OWLNamedIndividual i : dogistances.getFlattened()) {
					System.out.println(i.getIRI().getFragment());
					myText.append("\n" + i.getIRI().getFragment());
					dogname = i;
				}
			}
		}
		myText.append("\n\n\n superclassi di: " + dogname + "\n\n\n" + reasoner.getTypes(dogname, false) + "\n\n");

		
		
		
		startTime = System.nanoTime();
		NodeSet<OWLClass> duckclasses = reasoner.getTypes(duckname, false);
		NodeSet<OWLClass> dogclasses = reasoner.getTypes(dogname, false);
	
		
		ArrayList<OWLClass> nodesA = new ArrayList<OWLClass>();
		for (Node<OWLClass> c : duckclasses) {
			OWLClass a = c.getRepresentativeElement();
			nodesA.add(a);
		}	
		myText.append("\n\n  duck tree \n\n" );
		for (OWLClass c : nodesA) {
			myText.append("\n\n" + c);
		}
		myText.append("\n\n\n\n\n\n  ---------------------- \n");

		
		
		
		
		ArrayList<OWLClass> nodesB = new ArrayList<OWLClass>();
		for (Node<OWLClass> c : dogclasses) {
			OWLClass b = c.getRepresentativeElement();
			nodesB.add(b);
		}
		myText.append("\n\n  dog tree \n\n" );
		for (OWLClass c : nodesB) {
			myText.append("\n\n" + c);
		}
		myText.append("\n\n\n\n\n\n  ---------------------- \n");
		
		
		
		nodesA.retainAll(nodesB);

		myText.append("\n\n common classes  \n\n" );
		for (OWLClass c : nodesA) {
			myText.append("\n\n" + c);
		}

		
		
		ArrayList<OWLSubClassOfAxiom> finals = new ArrayList<OWLSubClassOfAxiom>();
		List<OWLSubClassOfAxiom> subsumptions = extractSubsumptions(reasoner, false, false, peopleOntology, fac);
		for (OWLSubClassOfAxiom subsumption : subsumptions) {
			System.out.println("" + subsumption);
			for (OWLClass c : nodesA) {
				if (c.equals(subsumption.getSubClass().asOWLClass())) {
					finals.add(subsumption);
				
				}
			}

		}
		ArrayList<OWLClass> nodesNotLeastCommon = new ArrayList<OWLClass>();
		for (OWLSubClassOfAxiom a : finals) {
			for (OWLClass c : nodesA) {
				if (c.equals(a.getSuperClass().asOWLClass())) {
					// myText.append("\n\n\n\n\n\n nodo scartato : CLASS " + c);
					nodesNotLeastCommon.add(c);
				}
			}

		}
		nodesA.removeAll(nodesNotLeastCommon);
		for (OWLClass c : nodesA) {
			if (c.isOWLThing() && nodesA.size() == 1) {
				myText.append("\n\n No common superclass  \n");
				break;
			} else if (c.isOWLThing() && nodesA.size() > 1) {
				continue;
			} else {
				myText.append("\n\n\n\n\n\n LEAST COMMON CLASS  \n" + c);
			}
		}
		endTime = System.nanoTime();
		duration = (endTime - startTime) / 1000000;
		myText.append("\n----------Execution Time : " + duration + "ms----------\n\n");
	}


	private static List<OWLSubClassOfAxiom> extractSubsumptions(final OWLReasoner reasoner, final boolean onlyDirect,
			final boolean onlyUntold, final OWLOntology ontology, final OWLDataFactory factory) {

		final Set<Node<OWLClass>> done = new HashSet<Node<OWLClass>>();
		final Queue<Node<OWLClass>> toDo = new LinkedList<Node<OWLClass>>();
		final List<OWLSubClassOfAxiom> result = new ArrayList<OWLSubClassOfAxiom>();

		toDo.add(reasoner.getTopClassNode());
		done.add(reasoner.getTopClassNode());

		Node<OWLClass> node;
		while ((node = toDo.poll()) != null) {

			if (!node.isBottomNode()) {

				// Put the equivalences into the conclusion queue.
				final ArrayList<OWLClass> entities = new ArrayList<OWLClass>(node.getEntities());
				for (int i = 0; i < entities.size() - 1; i++) {
					for (int j = 1; j < entities.size(); j++) {
						final OWLClass first = entities.get(i);
						final OWLClass second = entities.get(j);

						if (first.equals(second)) {
							continue;
						}

						if (!second.equals(factory.getOWLThing()) && !first.equals(factory.getOWLNothing())) {
							final OWLSubClassOfAxiom axiom = factory.getOWLSubClassOfAxiom(first, second);
							if (!onlyUntold || !isAsserted(axiom, ontology)) {
								result.add(axiom);
							}
						}

						if (!first.equals(factory.getOWLThing()) && !second.equals(factory.getOWLNothing())) {
							final OWLSubClassOfAxiom axiom = factory.getOWLSubClassOfAxiom(second, first);
							if (!onlyUntold || !isAsserted(axiom, ontology)) {
								result.add(axiom);
							}
						}

					}
				}

				// Put the subclasses into the conclusion queue.
				for (final OWLClass sup : entities) {
					if (sup.equals(factory.getOWLThing())) {
						continue;
					}
					for (final Node<OWLClass> subNode : reasoner.getSubClasses(node.getRepresentativeElement(),
							onlyDirect)) {
						if (subNode.isBottomNode()) {
							continue;
						}
						for (final OWLClass sub : subNode) {
							final OWLSubClassOfAxiom axiom = factory.getOWLSubClassOfAxiom(sub, sup);
							if (!onlyUntold || !isAsserted(axiom, ontology)) {
								result.add(axiom);
							}
						}
					}
				}

			}

			// Queue up the subnodes.
			for (final Node<OWLClass> subNode : reasoner.getSubClasses(node.getRepresentativeElement(), true)) {
				if (done.add(subNode)) {
					toDo.add(subNode);
				}
			}

		}

		return result;
	}

	private static boolean isAsserted(final OWLSubClassOfAxiom axiom, final OWLOntology ontology) {
		final Set<OWLSubClassOfAxiom> axioms = ontology.getSubClassAxiomsForSubClass(axiom.getSubClass().asOWLClass());
		if (axioms == null || axioms.isEmpty()) {
			return false;
		} else {
			return axioms.contains(axiom);
		}
	}

}
